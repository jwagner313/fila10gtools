% GNU Octave script
%
% Helper script to calculate chan_perm register values
% to change 2-bit encoding from K5/VLBA/MkIV into VDIF
% sample encoding.


% FILA10G Command:  
%   regwrite chan_perm <regnr> <mapping>
% where <regnr> is register number from 0 to 7 or more
% and <mapping> is some value from 0x00000000 to 0xFFFFFFFF
% 
% Inside FILA10G FPGA, the VSI Port input bits are copied to Internal
% Output register bits. The Internal Output is later copied into VDIF.
%
% You can tell which input bit to copy to which output bit:
%
% The <regnr> is related to Output Bit.
% Reg nr 0 contains mapping for output bits 3,2,1,0.
% Reg nr 1 contains mapping for output bits 7,6,5,4.
% Reg nr 2 contains mapping for output bits 11,10,9,8
% ...
%
% The <mapping> is 4 bytes (written as a 32-bit word) that are the
% index of the input bit. For a single VSI Input Port there are 32
% input bits so the index values can be 0x00 to 0x1F.
% 
% Example: regwrite chan_perm 0 0x03020100
% means that
%    output bits 3,2,1,0   
%       are taken from   
%    input bits 3 (0x03), 2 (0x02), 1 (0x01) and 0 (0x00)  
% This 0x03020100 just copies the data in the first 4 inputs bits 
% directly into the same place in the output, without changes in 
% the bit order.

% Sample encoding:
%  K5/VLBA/MkIV:  00, 10, 01, 11 = -HiMag, -1.0, +1.0, +HiMag 
%  VDIF:          00, 01, 10, 11 = -HiMag, -1.0, +1.0, +HiMag

N = 32; % number of input bits

% make default chan_perm mapping
xx = 0:(N-1);
xx = reshape(xx,[4 N/4]);

% change sample encoding (mirror 2-bit values)
xxswapped = [xx(2,:); xx(1,:); xx(4,:); xx(3,:)];

% print the new chan_perm in Text form
for regnr = 0:(N/4-1),
    mapping = xxswapped(:,regnr+1);
    fprintf(1, 'regwrite chan_perm %d 0x%02X%02X%02X%02X\n', ...
      regnr, mapping(4),mapping(3),mapping(2),mapping(1));
end




%% Same for full 128-bit 4-VSI input

N = 128; % num input VSI bits
xx=0:(N-1); xx=reshape(xx,[4 N/4]);
% default (no conversion)
for regnr=0:(N/4-1),
    mapping = xx(:,regnr+1);
	fprintf(1, '%% regwrite chan_perm %d 0x%02X%02X%02X%02X\n', regnr, mapping(4),mapping(3),mapping(2),mapping(1));
end

%% Without any conversion, direct data copy:
% regwrite chan_perm 0 0x03020100
% regwrite chan_perm 1 0x07060504
% regwrite chan_perm 2 0x0B0A0908
% regwrite chan_perm 3 0x0F0E0D0C
% regwrite chan_perm 4 0x13121110
% regwrite chan_perm 5 0x17161514
% regwrite chan_perm 6 0x1B1A1918
% regwrite chan_perm 7 0x1F1E1D1C
% regwrite chan_perm 8 0x23222120
% regwrite chan_perm 9 0x27262524
% regwrite chan_perm 10 0x2B2A2928
% regwrite chan_perm 11 0x2F2E2D2C
% regwrite chan_perm 12 0x33323130
% regwrite chan_perm 13 0x37363534
% regwrite chan_perm 14 0x3B3A3938
% regwrite chan_perm 15 0x3F3E3D3C
% regwrite chan_perm 16 0x43424140
% regwrite chan_perm 17 0x47464544
% regwrite chan_perm 18 0x4B4A4948
% regwrite chan_perm 19 0x4F4E4D4C
% regwrite chan_perm 20 0x53525150
% regwrite chan_perm 21 0x57565554
% regwrite chan_perm 22 0x5B5A5958
% regwrite chan_perm 23 0x5F5E5D5C
% regwrite chan_perm 24 0x63626160
% regwrite chan_perm 25 0x67666564
% regwrite chan_perm 26 0x6B6A6968
% regwrite chan_perm 27 0x6F6E6D6C
% regwrite chan_perm 28 0x73727170
% regwrite chan_perm 29 0x77767574
% regwrite chan_perm 30 0x7B7A7978
% regwrite chan_perm 31 0x7F7E7D7C

%% 2-bit converted
xxswapped = [xx(2,:); xx(1,:); xx(4,:); xx(3,:)];
for regnr=0:(N/4-1),
    mapping = xxswapped(:,regnr+1);
	fprintf(1, '%% regwrite chan_perm %d 0x%02X%02X%02X%02X\n', regnr, mapping(4),mapping(3),mapping(2),mapping(1));
end
%# VSI1
% regwrite chan_perm 0 0x02030001
% regwrite chan_perm 1 0x06070405
% regwrite chan_perm 2 0x0A0B0809
% regwrite chan_perm 3 0x0E0F0C0D
% regwrite chan_perm 4 0x12131011
% regwrite chan_perm 5 0x16171415
% regwrite chan_perm 6 0x1A1B1819
% regwrite chan_perm 7 0x1E1F1C1D
%# VSI2
% regwrite chan_perm 8 0x22232021
% regwrite chan_perm 9 0x26272425
% regwrite chan_perm 10 0x2A2B2829
% regwrite chan_perm 11 0x2E2F2C2D
% regwrite chan_perm 12 0x32333031
% regwrite chan_perm 13 0x36373435
% regwrite chan_perm 14 0x3A3B3839
% regwrite chan_perm 15 0x3E3F3C3D
%# VSI3
% regwrite chan_perm 16 0x42434041
% regwrite chan_perm 17 0x46474445
% regwrite chan_perm 18 0x4A4B4849
% regwrite chan_perm 19 0x4E4F4C4D
% regwrite chan_perm 20 0x52535051
% regwrite chan_perm 21 0x56575455
% regwrite chan_perm 22 0x5A5B5859
% regwrite chan_perm 23 0x5E5F5C5D
%# VSI4
% regwrite chan_perm 24 0x62636061
% regwrite chan_perm 25 0x66676465
% regwrite chan_perm 26 0x6A6B6869
% regwrite chan_perm 27 0x6E6F6C6D
% regwrite chan_perm 28 0x72737071
% regwrite chan_perm 29 0x76777475
% regwrite chan_perm 30 0x7A7B7879
% regwrite chan_perm 31 0x7E7F7C7D



%% 4 IF grouping together with 2-bit sample conversion
%  Input 128-bit data
%  VSI1: bits 0-31, VSI2: bits 32-63, VSI3: bits 64-95, VSI4: bits 96-127
%        0x00-0x1F         0x20-0x3F        0x40-0x5F         0x60-0x7F
% Gino:
%  " regwrite chan_perm 0 0x21200100
%    regwrite chan_perm 1 0x61604140
%    regwrite chan_perm 2 0x23220302
%    regwrite chan_perm 3 0x63624342 ... "
% t0: 0,1+0, 32,33+0, 64,65+0, 96,97+0 = if1 if2 if3 if4
% t1: 0,1+2, 32,33+2, 64,65+2, 96,97+2 = if1 if2 if3 if4
% ...
N = 128; % 4 x VSI
xx = [];
timeN = [0 1  32 33  64 65  96 97];  % IF grouping
for tt=1:16,
    xx = [xx, timeN];
    timeN = timeN + 2;
end
xx=reshape(xx,[4 N/4]);
xxgroupedswapped=[xx(2,:); xx(1,:); xx(4,:); xx(3,:)];  % 2-bit swap

for regnr=0:(N/4-1),
    mapping = xxgroupedswapped(:,regnr+1);
	fprintf(1, '%% regwrite chan_perm %d 0x%02X%02X%02X%02X\n', regnr, mapping(4),mapping(3),mapping(2),mapping(1));
end

% regwrite chan_perm 0 0x20210001
% regwrite chan_perm 1 0x60614041
% regwrite chan_perm 2 0x22230203
% regwrite chan_perm 3 0x62634243
% regwrite chan_perm 4 0x24250405
% regwrite chan_perm 5 0x64654445
% regwrite chan_perm 6 0x26270607
% regwrite chan_perm 7 0x66674647
% regwrite chan_perm 8 0x28290809
% regwrite chan_perm 9 0x68694849
% regwrite chan_perm 10 0x2A2B0A0B
% regwrite chan_perm 11 0x6A6B4A4B
% regwrite chan_perm 12 0x2C2D0C0D
% regwrite chan_perm 13 0x6C6D4C4D
% regwrite chan_perm 14 0x2E2F0E0F
% regwrite chan_perm 15 0x6E6F4E4F
% regwrite chan_perm 16 0x30311011
% regwrite chan_perm 17 0x70715051
% regwrite chan_perm 18 0x32331213
% regwrite chan_perm 19 0x72735253
% regwrite chan_perm 20 0x34351415
% regwrite chan_perm 21 0x74755455
% regwrite chan_perm 22 0x36371617
% regwrite chan_perm 23 0x76775657
% regwrite chan_perm 24 0x38391819
% regwrite chan_perm 25 0x78795859
% regwrite chan_perm 26 0x3A3B1A1B
% regwrite chan_perm 27 0x7A7B5A5B
% regwrite chan_perm 28 0x3C3D1C1D
% regwrite chan_perm 29 0x7C7D5C5D
% regwrite chan_perm 30 0x3E3F1E1F
% regwrite chan_perm 31 0x7E7F5E5F


%% 2 IF grouping together with 2-bit sample conversion
%  Input 64-bit data
%  VSI1: bits 0-31, VSI2: bits 32-63
%        0x00-0x1F         0x20-0x3F
% t0,1: 0,1+0, 32,33+0, 0,1+2,  32,33+2 = if1_t0 if2_t0 if1_t1 if2_t1
% ...
N = 64; % 2 x VSI
xx = [];
timeN = [0 1  32 33];  % IF grouping
for tt=1:16,
    xx = [xx, timeN];
    timeN = timeN + 2;
end
xx=reshape(xx,[4 N/4]);
xxgroupedswapped=[xx(2,:); xx(1,:); xx(4,:); xx(3,:)];  % 2-bit swap

for regnr=0:(N/4-1),
    mapping = xxgroupedswapped(:,regnr+1);
	fprintf(1, '%% regwrite chan_perm %d 0x%02X%02X%02X%02X\n', regnr, mapping(4),mapping(3),mapping(2),mapping(1));
end

% regwrite chan_perm 0 0x20210001
% regwrite chan_perm 1 0x22230203
% regwrite chan_perm 2 0x24250405
% regwrite chan_perm 3 0x26270607
% regwrite chan_perm 4 0x28290809
% regwrite chan_perm 5 0x2A2B0A0B
% regwrite chan_perm 6 0x2C2D0C0D
% regwrite chan_perm 7 0x2E2F0E0F
% regwrite chan_perm 8 0x30311011
% regwrite chan_perm 9 0x32331213
% regwrite chan_perm 10 0x34351415
% regwrite chan_perm 11 0x36371617
% regwrite chan_perm 12 0x38391819
% regwrite chan_perm 13 0x3A3B1A1B
% regwrite chan_perm 14 0x3C3D1C1D
% regwrite chan_perm 15 0x3E3F1E1F

