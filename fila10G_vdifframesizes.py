#!/usr/bin/python

bw = 16e6      # single IF bandwidth
Nif = 16       # number of IFs
Lmax = 9000    # jumbo frame size (Ethernet) 

rate = 2*bw * Nif * 2  # bit/s
bytepersec = rate/8.0

# max payload = max Eth jumbo frame - max empty UDP/IP - max VDIF header length
Lmax = Lmax - 52 - 32    

print 'Example data rate of %f Mbps = %d byte/second' % (rate*1e-6,bytepersec)
print 'Available VDIF frame sizes that give an integer number of frames per second:'

bytepersec = int(bytepersec)
for nb in range(128,Lmax):
	if (bytepersec % nb) == 0:
		framespersec = bytepersec / nb
		if (framespersec % Nif) == 0:
			ctok = 'yes'
		else:
			ctok = 'no'
		print '  %4d+32 byte : %7d frames/sec : %d-ch cornerturn %s' % (nb,framespersec,Nif,ctok)


print 'The FILA10G adds a 8-byte packet sequence number into the UDP/IP packet,'
print 'followed by the actual VDIF frame.'


