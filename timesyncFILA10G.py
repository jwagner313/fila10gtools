#!/usr/bin/python
#
# Python version of FILA10G clock synchronization to UT time
# based on local (NTP) time of this computer. This script
# assumes a TCP or UDP connection (RS232<->Ethernet converter)
# rather than serial port is used.
#
# Example: ./timesyncFILA10G.py 134.104.25.21 23
#
# Note: syntax of 'timesync' sent to FILA10G might
# need to be updated to match newer FILA10G firmware!
#

import os
import time
import math
import sys
import socket


# Wait to avoid a minute-boundary, then wait until second changes
def wait_safe():
	while True:
		utc = time.gmtime()
		if (utc.tm_sec<55):
			break
	while True:
		utc1 = time.gmtime()
		utc2 = time.gmtime()
		if not(utc1.tm_sec==utc2.tm_sec):
			break

# Return a socket:  conn=(ip,port,use_tcp)
def getSocket(conn):
	try:
		if conn['isTCP']:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect((conn['ip'],conn['port']))
		else:
			s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.settimeout(1.0)
		conn['socket'] = s
	except:
		print 'Connection to %s:%u failed' % (conn['ip'],conn['port'])
		return None
	return conn

# Send a command, wait for reply
def sendRcv(conn,cmdstr):
	buffer_size = 2048
	if conn['isTCP']:
		conn['socket'].send(cmdstr)
	else:
		conn['socket'].sendto(cmdstr, (conn['ip'],conn['port']))
	reply = ''
	try:
		while True:
			data = conn['socket'].recv(buffer_size)
			reply = reply + data
	except:
		print 'Reply: %s' % (reply.strip())
		# print 'Timed out with no reply'

# Convert time struct to (MJD,julian)
def convert_utc(utc):

	L = math.ceil((utc.tm_mon - 14) / 12.0); # In leap years, -1 for Jan, Feb, else 0
	p1 = int(utc.tm_mday - 32075 + math.floor(1461.0 * (utc.tm_year + 4800 + L) / 4))
	p2 = math.floor(367.0 * (utc.tm_mon - 2 - L * 12) / 12)
	p3 = 3 * math.floor( math.floor((utc.tm_year + 4900 + L) / 100.0) / 4.0 )
 	julian = float(p1 + p2 - p3)
	julian = julian + (utc.tm_hour / 24.0) - 0.5;
	MJD = int(julian - 2400000.5)
	return (MJD,julian)

# Convert MJD time to Mark5B format
def convert_utc2mark5b(MJD, utc):
	
	MJD = MJD % (999 + 1) # 3-digit BCD
	yearsSince2k = abs(utc.tm_year - 2000) % 16 # 4bit binary years since 2000
	secOfDay = int(utc.tm_sec) + 60*utc.tm_min + 60*60*utc.tm_hour # 5-digit BCD
	return (MJD,yearsSince2k,secOfDay)

def usage_exit():
	print 'Usage: timesyncFILA10G.py [-u|--udp] <ip_address> <ip_port>'
	sys.exit(-1)

# Prepare and send commands to FILA10G
def main(argv):

	if len(argv)<3:
		usage_exit()

	if (argv[1][0]=='-'):
		if (argv[1]=='--udp' or argv[1]=='-u') and len(argv)==4:
			conn_TCP = False
			conn_IPaddr = str(argv[2])
			conn_IPport = int(argv[3])	
		else:
			usage_exit()
	else:
		conn_TCP = True
		conn_IPaddr = str(argv[1])
		conn_IPport = int(argv[2])
	conn = {'ip':conn_IPaddr, 'port':conn_IPport, 'isTCP':conn_TCP, 'socket':None}

	print 'Connecting...'
	conn = getSocket(conn)
	if (conn==None):
		sys.exit(-1)

	print 'Waiting to avoid minute boundary...'
	wait_safe()

	if False:
		print 'Using TEST date 2010-10-05 16:21:42'
		print 'Expected result: MJD=474, secofday=58902'
		utc = time.strptime('2010-10-05 16:21:42', '%Y-%m-%d %H:%M:%S')
	else:
		utc = time.gmtime()

	MJD,julian = convert_utc(utc)
	mk5MJD,y2k,secofday = convert_utc2mark5b(MJD, utc)

	print ''
	print 'Using date and time: ', utc
	print 'This converts to MJD=%u julian=%8.2e' % (MJD,julian)
	print 'For the Mark5B headers this is MJD=%u year=%u secofday=%u' % (mk5MJD,y2k,secofday)
	print ''

	# Register 14: FILA10G year (reg bits 3..0) + Mark5B User Info field (reg bits 27...16)
	filareg14 = y2k + (0x00000EAD << 16)

	# Commands
	cmds=[]
	cmds.append('\r\nregwrite regbank0 14 %u\r\n' % (filareg14))    # set year and user info
	cmds.append('timesync %u %u \r\n' % (secofday,mk5MJD))	        # reload date on next 1PPS
	cmds.append('reset\r\n')                                        # clear FIFOs, resync again to 1PPS

	print 'The following commands will be sent:'
	for cmd in cmds:
		print 'Command: ', cmd.strip()
	for cmd in cmds:
		sendRcv(conn,cmd)

	print 'Done!'

if __name__ == "__main__":
    main(sys.argv)

