# Configuration for Four-IF input with single IF's on each VSI.
# The configuration will group the IF samples to produce 4-IF VDIF data.
# This configuration does not apply any 2-bit sample conversion.

# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.20
tengbcfg eth1 ip=10.10.1.20

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:DD:44:2B:42 and :43
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:43
tengbarp eth1 1 00:60:DD:44:2B:42

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.1:46227

splitmode off
inputselect vsi1-2-3-4
vsi_samplerate 64000000

# Data permutation: do not change 2-bit encoding,
# but change the order of VSI samples.
#  VSI1: bits 0-31, VSI2: bits 32-63, VSI3: bits 64-95, VSI4: bits 96-127
#        0x00-0x1F         0x20-0x3F        0x40-0x5F         0x60-0x7F
regwrite chan_perm 0 0x21200100
regwrite chan_perm 1 0x61604140
regwrite chan_perm 2 0x23220302
regwrite chan_perm 3 0x63624342
regwrite chan_perm 4 0x25240504
regwrite chan_perm 5 0x65644544
regwrite chan_perm 6 0x27260706
regwrite chan_perm 7 0x67664746
regwrite chan_perm 8 0x29280908
regwrite chan_perm 9 0x69684948
regwrite chan_perm 10 0x2B2A0B0A
regwrite chan_perm 11 0x6B6A4B4A
regwrite chan_perm 12 0x2D2C0D0C
regwrite chan_perm 13 0x6D6C4D4C
regwrite chan_perm 14 0x2F2E0F0E
regwrite chan_perm 15 0x6F6E4F4E
regwrite chan_perm 16 0x31301110
regwrite chan_perm 17 0x71705150
regwrite chan_perm 18 0x33321312
regwrite chan_perm 19 0x73725352
regwrite chan_perm 20 0x35341514
regwrite chan_perm 21 0x75745554
regwrite chan_perm 22 0x37361716
regwrite chan_perm 23 0x77765756
regwrite chan_perm 24 0x39381918
regwrite chan_perm 25 0x79785958
regwrite chan_perm 26 0x3B3A1B1A
regwrite chan_perm 27 0x7B7A5B5A
regwrite chan_perm 28 0x3D3C1D1C
regwrite chan_perm 29 0x7D7C5D5C
regwrite chan_perm 30 0x3F3E1F1E
regwrite chan_perm 31 0x7F7E5F5E
reset

vdif_station KT
vdif_frame 2 4 1280 ct=off
# regupdate vdif_header 2 0x02000000 0x1F000000
# regupdate vdif_header 3 0x04000000 0x7F000000

## auto_timesync_fila10g
start vdif
