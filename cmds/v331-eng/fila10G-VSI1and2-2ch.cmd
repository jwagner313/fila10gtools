# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.20
tengbcfg eth1 ip=10.10.1.20

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:DD:44:2B:42 and :43
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:43
tengbarp eth1 1 00:60:DD:44:2B:42

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.1:46227

vsi_samplerate 64000000
inputselect vsi1-2
splitmode off
reset

vdif_station KT
vdif_frame 2 2 1280 ct=off 

# Register tweak to mirror every 2-bits to change from ADS-1000/K5/VLBA
# sample encoding into VDIF standard sample encoding
chan_perm 0 0x02030001
chan_perm 1 0x06070405
chan_perm 2 0x0A0B0809
chan_perm 3 0x0E0F0C0D
chan_perm 4 0x12131011
chan_perm 5 0x16171415
chan_perm 6 0x1A1B1819
chan_perm 7 0x1E1F1C1D
chan_perm 8 0x22232021
chan_perm 9 0x26272425
chan_perm 10 0x2A2B2829
chan_perm 11 0x2E2F2C2D
chan_perm 12 0x32333031
chan_perm 13 0x36373435
chan_perm 14 0x3A3B3839
chan_perm 15 0x3E3F3C3D

# The following line (with # comment markers!) will trigger a time synchronization
## auto_timesync_fila10g

start vdif


