# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.20
tengbcfg eth1 ip=10.10.1.20

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:DD:44:2B:42 and :43
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:43
tengbarp eth1 1 00:60:DD:44:2B:42

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.1:46227

# Input config
vsi_samplerate 64000000
inputselect vsi1
splitmode off
reset

vdif_station KT
# vdif_frame 2 16 1280
vdif_frame 2 16 1280 ct=off 
# note: ct=off option supported by 3.3.1 engineering version of firmware

# Register tweak to mirror every 2-bits to change from ADS-1000/K5/VLBA 
# sample encoding into VDIF standard sample encoding
regwrite chan_perm 0 0x02030001
regwrite chan_perm 1 0x06070405
regwrite chan_perm 2 0x0A0B0809
regwrite chan_perm 3 0x0E0F0C0D
regwrite chan_perm 4 0x12131011
regwrite chan_perm 5 0x16171415
regwrite chan_perm 6 0x1A1B1819
regwrite chan_perm 7 0x1E1F1C1D

# The following line (with # comment markers!) will trigger a time synchronization
## auto_timesync_fila10g

start vdif

