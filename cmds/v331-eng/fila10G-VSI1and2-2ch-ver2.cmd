# Configuration for Two-IF input with single-channel IF's on each VSI.
# The configuration will group the IF samples to produce 4-IF VDIF data.
# This configuration also applies a 2-bit sample conversion.

# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.5
tengbcfg eth1 ip=10.10.1.21

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:dd:44:2b:30 and : 31
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:30
tengbarp eth1 1 00:60:dd:44:2b:31

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.17:46227

splitmode off
inputselect vsi1-2
vsi_samplerate 64000000

# Input:
#  VSI1 carries 16 samples of IF1, VSI2 carries 16 samples of IF2
#
# Interleave the data to group together the samples of the 2 IFs:
#  Input 64-bit data
#  VSI1: bits 0-31, VSI2: bits 32-63
#        0x00-0x1F         0x20-0x3F
#
# Also do 2-bit sample encoding conversion (exchange sign, mag bits)
regwrite chan_perm 0 0x20210001
regwrite chan_perm 1 0x22230203
regwrite chan_perm 2 0x24250405
regwrite chan_perm 3 0x26270607
regwrite chan_perm 4 0x28290809
regwrite chan_perm 5 0x2A2B0A0B
regwrite chan_perm 6 0x2C2D0C0D
regwrite chan_perm 7 0x2E2F0E0F
regwrite chan_perm 8 0x30311011
regwrite chan_perm 9 0x32331213
regwrite chan_perm 10 0x34351415
regwrite chan_perm 11 0x36371617
regwrite chan_perm 12 0x38391819
regwrite chan_perm 13 0x3A3B1A1B
regwrite chan_perm 14 0x3C3D1C1D
regwrite chan_perm 15 0x3E3F1E1F
reset

vdif_station KT
vdif_frame 2 2 1280 ct=off

# regupdate <device name> <register index> <register value> <bit mask value>
regupdate vdif_header 2 0x02000000 0x1F000000
regupdate vdif_header 3 0x02000000 0x7F000000

## auto_timesync_fila10g
start vdif
