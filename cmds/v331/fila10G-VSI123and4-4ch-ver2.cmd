# Configuration for Four-IF input with single-channel IF's on each VSI.
# The configuration will group the IF samples to produce 4-IF VDIF data.
# This configuration also applies a 2-bit sample conversion.

# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.20
tengbcfg eth1 ip=10.10.1.20

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:DD:44:2B:42 and :43
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:43
tengbarp eth1 1 00:60:DD:44:2B:42

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.1:46227

splitmode off
inputselect vsi1-2-3-4
vsi_samplerate 64000000

# Input:
#  VSI1 carries 16 samples of IF1, VSI2 carries 16 samples of IF2, 
#  VSI3 carries 16 samples of IF3, VSI4 carries 16 samples of IF4
#  = 128 bit that are not in {t0/IF1,t0/IF2,t0/IF3,t0/IF4, t1/IF1,T1/IF2, ...} order
#    but rather in {t0/IF1,...,t15/IF1, t0/IF2,..,t15/IF2, ...} order
#
# Interleave the data to group together the samples of the 4 IFs:
#  Input 128-bit data
#  VSI1: bits 0-31, VSI2: bits 32-63, VSI3: bits 64-95, VSI4: bits 96-127
#        0x00-0x1F         0x20-0x3F        0x40-0x5F         0x60-0x7F
#  Output 128-bit data
#
# Also do 2-bit sample encoding conversion (exchange sign, mag bits)
regwrite chan_perm 0 0x20210001
regwrite chan_perm 1 0x60614041
regwrite chan_perm 2 0x22230203
regwrite chan_perm 3 0x62634243
regwrite chan_perm 4 0x24250405
regwrite chan_perm 5 0x64654445
regwrite chan_perm 6 0x26270607
regwrite chan_perm 7 0x66674647
regwrite chan_perm 8 0x28290809
regwrite chan_perm 9 0x68694849
regwrite chan_perm 10 0x2A2B0A0B
regwrite chan_perm 11 0x6A6B4A4B
regwrite chan_perm 12 0x2C2D0C0D
regwrite chan_perm 13 0x6C6D4C4D
regwrite chan_perm 14 0x2E2F0E0F
regwrite chan_perm 15 0x6E6F4E4F
regwrite chan_perm 16 0x30311011
regwrite chan_perm 17 0x70715051
regwrite chan_perm 18 0x32331213
regwrite chan_perm 19 0x72735253
regwrite chan_perm 20 0x34351415
regwrite chan_perm 21 0x74755455
regwrite chan_perm 22 0x36371617
regwrite chan_perm 23 0x76775657
regwrite chan_perm 24 0x38391819
regwrite chan_perm 25 0x78795859
regwrite chan_perm 26 0x3A3B1A1B
regwrite chan_perm 27 0x7A7B5A5B
regwrite chan_perm 28 0x3C3D1C1D
regwrite chan_perm 29 0x7C7D5C5D
regwrite chan_perm 30 0x3E3F1E1F
regwrite chan_perm 31 0x7E7F5E5F
reset

vdif_station KT
# vdif_frame 2 4 1280 ct=off  not supported by official v3.3
vdif_frame 32 4 1280
regupdate vdif_header 2 0x02000000 0x1F000000
regupdate vdif_header 3 0x04000000 0x7F000000

## auto_timesync_fila10g
start vdif
