# Configuration for Single-IF input from single-channel IF's on each VSI.
# Produces VDIF with 8224 byte/frame, 8192-byte payload.
# The 1 IF x 16 samples x 2-bit data have originally ADS-1000 sample encoding
# which is converted here (swap sign&magnitude bits) to get standard VDIF samples.
reboot

# Configure
stop
vsi_swap reset
reset
tengbcfg eth0 ip=10.10.1.5
tengbcfg eth1 ip=10.10.1.21

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:dd:44:2b:30 and : 31
# or           00:60:dd:43:8d:b0 and : b1
#
arp off
tengbarp eth0 1 00:60:dd:43:8d:b0
tengbarp eth1 17 00:60:dd:43:8d:b1

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.17:46227

# Single VSI input
splitmode off
inputselect vsi1
vsi_samplerate 64000000

# Register tweak to mirror every 2-bits to change from ADS-1000/K5/VLBA
# sample encoding into VDIF standard sample encoding
regwrite chan_perm 0 0x02030001
regwrite chan_perm 1 0x06070405
regwrite chan_perm 2 0x0A0B0809
regwrite chan_perm 3 0x0E0F0C0D
regwrite chan_perm 4 0x12131011
regwrite chan_perm 5 0x16171415
regwrite chan_perm 6 0x1A1B1819
regwrite chan_perm 7 0x1E1F1C1D
reset
vdif_station KU
vdif_frame 2 1 8192 ct=off

# The following line (with # comment markers!) will trigger a time synchronization
## auto_timesync_fila10g

start vdif
