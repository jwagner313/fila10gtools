# Configure
stop
reset
tengbcfg eth0 ip=10.10.1.5
tengbcfg eth1 ip=10.10.1.21

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:DD:44:2B:42 and :43
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:2e
tengbarp eth1 1 00:60:dd:44:2b:2f

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.17:46227

# Single VSI input
vsi_samplerate 64000000
inputselect vsi2
splitmode off

# Register tweak to mirror every 2-bits to change from ADS-1000/K5/VLBA
# sample encoding into VDIF standard sample encoding
regwrite chan_perm 8 0x23222120
regwrite chan_perm 9 0x27262524
regwrite chan_perm 10 0x2B2A2928
regwrite chan_perm 11 0x2F2E2D2C
regwrite chan_perm 12 0x33323130
regwrite chan_perm 13 0x37363534
regwrite chan_perm 14 0x3B3A3938
regwrite chan_perm 15 0x3F3E3D3C
reset
vdif_station KU
vdif_frame 2 1 8192 ct=off

# The following line (with # comment markers!) will trigger a time synchronization
## auto_timesync_fila10g

start vdif
