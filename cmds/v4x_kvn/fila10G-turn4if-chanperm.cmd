# Input:
#  VSI1 carries 16 samples of IF1, VSI2 carries 16 samples of IF2, 
#  VSI3 carries 16 samples of IF3, VSI4 carries 16 samples of IF4
#  = 128 bit that are not in {t0/IF1,t0/IF2,t0/IF3,t0/IF4, t1/IF1,T1/IF2, ...} order
#    but rather in {t0/IF1,...,t15/IF1, t0/IF2,..,t15/IF2, ...} order
#
# Interleave the data to group together the samples of the 4 IFs:
#  Input 128-bit data
#  VSI1: bits 0-31, VSI2: bits 32-63, VSI3: bits 64-95, VSI4: bits 96-127
#        0x00-0x1F         0x20-0x3F        0x40-0x5F         0x60-0x7F
#  Output 128-bit data
regwrite chan_perm 0 0x21200100
regwrite chan_perm 1 0x61604140
regwrite chan_perm 2 0x23220302
regwrite chan_perm 3 0x63624342
regwrite chan_perm 4 0x25240504
regwrite chan_perm 5 0x65644544
regwrite chan_perm 6 0x27260706
regwrite chan_perm 7 0x67664746
regwrite chan_perm 8 0x29280908
regwrite chan_perm 9 0x69684948
regwrite chan_perm 10 0x2B2A0B0A
regwrite chan_perm 11 0x6B6A4B4A
regwrite chan_perm 12 0x2D2C0D0C
regwrite chan_perm 13 0x6D6C4D4C
regwrite chan_perm 14 0x2F2E0F0E
regwrite chan_perm 15 0x6F6E4F4E
regwrite chan_perm 16 0x31301110
regwrite chan_perm 17 0x71705150
regwrite chan_perm 18 0x33321312
regwrite chan_perm 19 0x73725352
regwrite chan_perm 20 0x35341514
regwrite chan_perm 21 0x75745554
regwrite chan_perm 22 0x37361716
regwrite chan_perm 23 0x77765756
regwrite chan_perm 24 0x39381918
regwrite chan_perm 25 0x79785958
regwrite chan_perm 26 0x3B3A1B1A
regwrite chan_perm 27 0x7B7A5B5A
regwrite chan_perm 28 0x3D3C1D1C
regwrite chan_perm 29 0x7D7C5D5C
regwrite chan_perm 30 0x3F3E1F1E
regwrite chan_perm 31 0x7F7E5F5E
