# Configuration for Four-IF input with single-channel IF's on each VSI.
#
# Produces VDIF with 8224 byte/frame, 8192-byte payload.
# The 4 IF x 16 samples x 2-bit data are packaged into VDIF directly.
# The VDIF payload thus consists of
#    [vsi4 byte 3..0 | vsi3 byte 3..0 | vsi2 byte 3..0 | vsi1 byte 3..0]
# with groups of 16-sample data from each IF, instead of the
# standard VDIF order. The sample 2-bit encoding is not converted.
#
# You can use 'shrinkVDIF' to extract the 16-sample groups of
# different IFs to produce more meaningful sample data, although
# the 2-bit encoding will remain wrong.

reboot

# Configure
stop
vsi_swap reset
reset
tengbcfg eth0 ip=10.10.1.5
tengbcfg eth1 ip=10.10.1.21

# Custom ARP (IP<->MAC Adress Resolution) table
#
# Example Mark6 at 10.10.1.1 has dual-port 10GbE
# with MACs of 00:60:dd:44:2b:30 and : 31
#
arp off
tengbarp eth0 1 00:60:dd:44:2b:2e
tengbarp eth1 17 00:60:dd:44:2b:2f

# Stream IP address destination
destination 0 10.10.1.1:46227
destination 1 10.10.1.17:46227

splitmode off
inputselect vsi1-2-3-4
vsi_samplerate 64000000

# Input:
#  VSI1 carries 16 samples of IF1, VSI2 carries 16 samples of IF2, 
#  VSI3 carries 16 samples of IF3, VSI4 carries 16 samples of IF4
#  = 128 bit that are not in {t0/IF1,t0/IF2,t0/IF3,t0/IF4, t1/IF1,T1/IF2, ...} order
#    but rather in {t0/IF1,...,t15/IF1, t0/IF2,..,t15/IF2, ...} order
# No channel permutation, just default 1:1 settings:
regwrite chan_perm 0 0x03020100
regwrite chan_perm 1 0x07060504
regwrite chan_perm 2 0x0B0A0908
regwrite chan_perm 3 0x0F0E0D0C
regwrite chan_perm 4 0x13121110
regwrite chan_perm 5 0x17161514
regwrite chan_perm 6 0x1B1A1918
regwrite chan_perm 7 0x1F1E1D1C
regwrite chan_perm 8 0x23222120
regwrite chan_perm 9 0x27262524
regwrite chan_perm 10 0x2B2A2928
regwrite chan_perm 11 0x2F2E2D2C
regwrite chan_perm 12 0x33323130
regwrite chan_perm 13 0x37363534
regwrite chan_perm 14 0x3B3A3938
regwrite chan_perm 15 0x3F3E3D3C
regwrite chan_perm 16 0x43424140
regwrite chan_perm 17 0x47464544
regwrite chan_perm 18 0x4B4A4948
regwrite chan_perm 19 0x4F4E4D4C
regwrite chan_perm 20 0x53525150
regwrite chan_perm 21 0x57565554
regwrite chan_perm 22 0x5B5A5958
regwrite chan_perm 23 0x5F5E5D5C
regwrite chan_perm 24 0x63626160
regwrite chan_perm 25 0x67666564
regwrite chan_perm 26 0x6B6A6968
regwrite chan_perm 27 0x6F6E6D6C
regwrite chan_perm 28 0x73727170
regwrite chan_perm 29 0x77767574
regwrite chan_perm 30 0x7B7A7978
regwrite chan_perm 31 0x7F7E7D7C
reset
vdif_station KT
vdif_frame 2 4 8192 ct=off

## auto_timesync_fila10g
start vdif
