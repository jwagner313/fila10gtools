//
// FILA10G internal clock synchronizer
// (C) 2010 Jan Wagner, GNU GPL
//
// Sets fila10g VLBA-style Mark5B clock to the current
// computer UTC time (Years since 2000, 3 last digits of MJD,
// and seconds of the day count)
//
// Uses the serial port class from
// http://hdrlab.org.nz/articles/windows-development/a-c-class-for-controlling-a-comm-port-in-windows/
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <cstdio>
#include <windows.h>
#include "Serial.h"
#define SER_BUF_SIZE 2048

int _tmain(int argc, _TCHAR* argv[])
{
	char buf[SER_BUF_SIZE];
	int MJD=0, secOfDay=0, yearsSince2k=0;
	SYSTEMTIME st;

	if (argc < 2) {
		fprintf(stderr, 
			"Sets the FILA10G clock to the current computer time\n"
			"in MJD and second-of-day format.\n\n"
			"Usage:   timesyncFILA10G <port>\n"
			"Example: timesyncFILA10G COM1\n"
		);
		return EXIT_FAILURE;
	}

	tstring portstr(argv[1]);
	try {

		Serial ser(portstr, 19200);
		ser.flush();

		// Get system time (UTC), avoiding a minute-boundary
		do {
			GetSystemTime(&st);
		} while(st.wSecond>55);

		// Wait until second changes
		int old_sec = st.wSecond;
		do {
			GetSystemTime(&st);
		} while(st.wSecond == old_sec);

		// Set time at the next 1PPS
		st.wSecond++; 

		#if 0 // Test case
		std::cout << "!! Using test case timestamp" << std::endl;
		std::cout << "Input: 2010-10-05 October 16:21:42" << std::endl;
		std::cout << "Expected result: MJD=474 TIME=16:21:42.0000=58902s" << std::endl;
		st.wSecond = 42;
		st.wMinute = 21;
		st.wHour = 16;
		st.wDay = 5;
		st.wMonth = 10;
		st.wYear = 2010;
		#endif

		// Convert time to integer MJD
		long L = (long)std::ceil((st.wMonth - 14) / 12.0); // In leap years, -1 for Jan, Feb, else 0
		long p1 = st.wDay - 32075 + (long)std::floor(1461.0 * (st.wYear + 4800 + L) / 4);
		long p2 = (long)std::floor (367.0 * (st.wMonth - 2 - L * 12) / 12);
		long p3 = 3 * (long)std::floor (std::floor ((st.wYear + 4900 + L) / 100.0) / 4.0);
		double julian = p1 + p2 - p3;
		julian = julian + (st.wHour / 24.0) - 0.5;
	    MJD = int(julian - 2400000.5);

		// Convert MJD time to Mark5B format
		MJD = MJD % (999 + 1); // 3-digit BCD
		yearsSince2k = abs(st.wYear - 2000) % 16; // 4bit binary years since 2000
		secOfDay = st.wSecond + 60*st.wMinute + 60*60*st.wHour; // 5-digit BCD

		// Configure FILA10G time
		_snprintf(buf, SER_BUF_SIZE, "\r\ntimesync %d %d \r\n", secOfDay, MJD);
		ser.write(buf);
		std::cout << buf;

		// Get response
		Sleep(1000);
		int nrd = ser.read(buf, SER_BUF_SIZE);
		std::cout << "Response: " << std::endl << buf;

		// Configure FILA10G year (Register 14 bits 3..0)
		// and Mark5B User Info field (bits 27...16)
		yearsSince2k = yearsSince2k + (0x00000EAD << 16);
		_snprintf(buf, SER_BUF_SIZE, "regwrite regbank0 14 %d\r\n", yearsSince2k);
		ser.write(buf);
		std::cout << buf;

		// Get response
		Sleep(1000);
		nrd = ser.read(buf, SER_BUF_SIZE);
		std::cout << "Response: " << std::endl << buf;


	} catch(const char *msg) {
		std::cout << msg << std::endl;
	}
	return 0;
}
