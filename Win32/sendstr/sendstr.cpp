//
// FILA10G serial command send
// (C) 2010 Jan Wagner, GNU GPL
//
// Could be made smarter. Maybe there are other free tools
// for serial port in-out.
//
// Uses the serial port class from
// http://hdrlab.org.nz/articles/windows-development/a-c-class-for-controlling-a-comm-port-in-windows/
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
//#include <cstdio>
#include <windows.h>
#include "Serial.h"
#define SER_BUF_SIZE 8192

int _tmain(int argc, _TCHAR* argv[])
{
	char buf[SER_BUF_SIZE];

	if (argc < 2) {
		std::cerr << 
			"Reads text lines from STDIN to send to serial port. Discards replys.\n\n"
			"Usage:   sendstr <port>\n"
			"Example: echo tengbcfg eth0 mac=01:23:45:67:89:ab | sendstr COM1\n" ;
		return EXIT_FAILURE;
	}

	tstring portstr(argv[1]);
	try {

		Serial ser(portstr, 19200);
		ser.flush();

		// Pipe STDIN
		while (1) {
			std::cin.getline(buf, SER_BUF_SIZE);
			if (std::string(buf).length()<=0) break;
			ser.write("\n");
			ser.write(buf);
			ser.write("\n");
			std::cout << "Sent: " << buf << std::endl;
			if (std::cin.eof()) break;
		}

		// Get response
		Sleep(1000);
		std::cout << "Response: " << std::endl;
		while (1) {
			int nrd = ser.read(buf, SER_BUF_SIZE, true);
			if (nrd==0) break;
			std::cout << buf;
			Sleep(1000);
		}
		std::cout << std::endl;

	} catch(const char *msg) {
		std::cout << msg << std::endl;
	}
	return 0;
}
