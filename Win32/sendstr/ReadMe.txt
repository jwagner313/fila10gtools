========================================================================
    CONSOLE APPLICATION : timesyncFILA10G Project Overview
========================================================================

AppWizard has created this timesyncFILA10G application for you.

This file contains a summary of what you will find in each of the files that
make up your timesyncFILA10G application.


timesyncFILA10G.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

timesyncFILA10G.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named timesyncFILA10G.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
