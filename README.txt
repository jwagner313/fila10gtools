--- consoleFILA10G.py ----------------------------------------------------

  Serial port console program. Can send a command file to the FILA10G at
  the start of the program. Can also do a time synchronization of the
  FILA10G to the computer time. The computer should be on NTP or PTP time.
  The program is based on the pySerial 2.6 'minicom' example program
  with a few minor additions for FILA10G. 

  Python and the pySerial need to be installed, e.g.,
    $ sudo aptitude install python-serial
  or on Windows
    1) Download and install Python 2.7
       Windows .exe or .msi installers are at 
       https://www.python.org/
    2) Download and install pyserial
       Windows .exe or .msi installers and source code are at 
       https://pypi.python.org/pypi/pyserial
       The installer may give an error "No Python installation found in the registry".
       In this case you can also download the source code (pyserial-2.7.tar.gz),
       unpack the file, open the command console (Win+R, cmd.exe), cd to the
       directory where pyserial-2.7 was unpacked and where setup.py is found,
       and do "python setup.py install" to install.

  Getting consoleFILA10G source code from the repository:
  $ git clone https://jwagner313@bitbucket.org/jwagner313/fila10gtools.git

  To later update the source code:
  $ git pull


  Usage:

  $ ./consoleFILA10G.py --cmd=cmds/fila10G-VSI1-16ch.cmd /dev/ttyUSB0

  and then Ctrl-T Ctrl-H for help. Time synchronization is 
  started with Ctrt-T Ctrl-S (or alternatively, Ctrl-T s). To exit 
  the program you can use Ctrl-] (or alternatively, Ctrl-T q). 

  There are a few *example* command files under the cmds/ subdirectory.

  Time sync can also be started from the cmd file using a commented line
  that contains "# auto_timesync_fila10g".

  An example command file:

    destination 0 192.168.1.20:46227
    destination 1 192.168.1.20:46227
    arp off
    
    # Single VSI input
    vsi_samplerate 64000000
    inputselect vsi1
    splitmode off
    reset
    
    vdif_station KT
    vdif_frame 2 1 1280 ct=off
    
    # Register tweak to mirror every 2-bits to change from ADS-1000/K5/VLBA
    # sample encoding into VDIF standard sample encoding
    regwrite chan_perm 0 0x02030001
    regwrite chan_perm 1 0x06070405
    regwrite chan_perm 2 0x0A0B0809
    regwrite chan_perm 3 0x0E0F0C0D
    regwrite chan_perm 4 0x12131011
    regwrite chan_perm 5 0x16171415
    regwrite chan_perm 6 0x1A1B1819
    regwrite chan_perm 7 0x1E1F1C1D
    
    # The following line (with # comment markers!) will trigger a time synchronization
    ## auto_timesync_fila10g
    
    start vdif



--- fila10G_vdifframesizes.py --------------------------------------------

  Lists VDIF payload sizes that result in an integer number
  of frames per second.

  Also tries to determine whether cornerturning (VSI input: 16 
  channels, VDIF setting:  "vdif_frame 2 1 1280" for 16 VDIF 
  Threads of 1-channel each) can be enabled and whether it
  produces an integer number of frames per second per Thread.
