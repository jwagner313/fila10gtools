# Reload Xilinx cable firmware
string=`lsusb -v | grep -i xilinx`     # "Bus 004 Device 007: ID 03fd:0008 Xilinx, Inc." or similar
IFS=' ' read -a array <<< "$string"    # split by whitespace --> array=(Bus, 004, Device, 007:, ...)
device="/dev/bus/usb/${array[1]:0:3}/${array[3]:0:3}"
echo "Loading xusb_xp2.hex firmware onto probable Xilinx cable at $device ..."
/sbin/fxload -v -t fx2 -I /usr/share/xusb_xp2.hex -D $device

# Reload windrvr6
/lib/modules/`uname -r`/kernel/drivers/misc/install_windrvr6 windrvr6 auto
chmod 666 /dev/windrvr6

# Reload parallel port driver
/lib/modules/`uname -r`/kernel/drivers/misc/install_xpc4drvr

