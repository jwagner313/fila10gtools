%
% function tvg32 = vsitvg(L)
%
% Returns a L-sample long sequence of VSI-H Test Vector Generator (TVG)
% data in 32-bit integer format (uint32). The TVG has a period of 32767
% samples, meaning that tvg32(i+1)==tvg32(i+32768).
%
% For reference, the expected 20 first TVG values are given in
% binary form in Table 13 of the VSI-H specifications. They are:
%  1 : 10111111111111110100000000000000
%  2 : 11001111111111110011000000000000
%  3 : 11101011111111110001010000000000
%  4 : 11110000111111110000111100000000
%  5 : 11111011101111110000010001000000
%  6 : 11111100110011110000001100110000
%  7 : 11111110101010110000000101010100
%  8 : 10111111000000000100000011111111
%  9 : 11001111101111110011000001000000
% 10 : 11101011110011110001010000110000
% 11 : 11110000111010110000111100010100
% 12 : 10111011101100000100010001001111
% 13 : 11001100110010110011001100110100
% 14 : 10101010101010000101010101010111
% 15 : 01000000000000011011111111111110
% 16 : 10001111111111110111000000000000
% 17 : 11011011111111110010010000000000
% 18 : 11100100111111110001101100000000
% 19 : 11110100101111110000101101000000
% 20 : 11111000100011110000011101110000
%
function tvg32 = vsitvg(L)

    if (L<=0), L=32767; end
    
    tvg32 = zeros(L,1,'uint32');
    tvg32(1) = hex2dec('BFFF4000');
    
    mask1 = uint32(hex2dec('0000ffff'));
    mask2 = uint16(hex2dec('3fff'));
    
%% Generate TVG sequence. Code based on C/C++ code.
    for i=(2:L),
        
        % C/C++: tvg16 = tvg[i-1] & 0x0000ffff;
        tvg16 = uint16(bitand(tvg32(i-1), mask1));

        % C/C++: tvg_register = tvg16 >> 1;
        tvg_register = uint16(bitshift(tvg16, -1)); 
        
        % C/C++: tvg16 = (tvg_register ^ (tvg_register >> 1));
        tvg16 = bitxor(tvg_register, bitshift(tvg_register, -1)); 

        % C/C++: tvg16 &= 0x3fff;
        tvg16 = bitand(tvg16, mask2);
        
        % C/C++: tvg16 |= ((tvg16 ^ (tvg16 >> 1)) << 15 );
        x = bitxor(tvg16, bitshift(tvg16,-1));
        x = bitshift(x, 15);
        tvg16 = bitor(tvg16, x); 
        
        % C/C++: tvg16 |= ( ((tvg_register >> 14) ^ tvg16) & 1) << 14;
        x = bitxor(bitshift(tvg_register,-14), tvg16);
        x = bitand(x, uint16(1));
        x = bitshift(x, 14);
        tvg16 = bitor(tvg16, x);
        
        % C/C++: tvg[i] = ((uint32_t)tvg16) | ((~(uint32_t)tvg16) << 16);
        x = bitshift(uint32(bitcmp(tvg16)), 16);
        tvg32(i) = bitor(uint32(tvg16), uint32(x));
        
    end

%% Binary printout of first 20 samples (debug)
    if 0,
        for i=1:min([L 20]),
            fprintf(1, '%4d : %s\n', i, dec2base(tvg32(i),2,32));
        end
    end

end
