%
% function fila10chipscopeVCD(vcdfile)
%
% Some checks on a VCD file exported from ChipScope after
% running ChipScope-enabled firmware on the FILA10G
% with all 4 VSI ports connected and capturing 2048
% timestamps.
%
% Shows the binary 'diff' between VSI1/2/3/4 assuming
% the FILA10G was connected to a VSI divider with
% binary identical data being input to the FILA10G on
% all its four VSI ports.
%
function fila10chipscopeVCD(vcdfile)

d = readVCD(vcdfile);   % save 'converted.mat' d; load 'converted.mat';

% Indices of the VSI bus databits of a ChipScope capture are defined in the ChipScope CDC file.
% The databits are unfortunately in lexical order (0,1,10..19,2,20..29, etc).
% We must re-order indices to extract the databits in numberical order (0..31).
if 0,
    % Choose only bits 0 to 15
    vsi1_databits_idx = 1 + [2 13 24 27 28 29 30 31 32 33 3 4 5 6 7 8];
    vsi2_databits_idx = 1 + [37 48 59 62 63 64 65 66 67 68 38 39 40 41 42 43];
    vsi3_databits_idx = 1 + [70 71 82 93 96 97 98 99 100 101 72 73 74 75 76 77];
    vsi4_databits_idx = 1 + [102 103 114 125 128 129 130 131 132 133 104 105 106 107 108 109];
else
    % Choose all bits 0 to 31
    vsi1_databits_idx = 1 + [2 13 24 27 28 29 30 31 32 33 3 4 5 6 7 8 , 9 10 11 12 14 15 16 17 18 19 20 21 22 23 25 26];
    vsi2_databits_idx = 1 + [37 48 59 62 63 64 65 66 67 68 38 39 40 41 42 43 , 44 45 46 47 49 50 51 52 53 54 55 56 57 58 60 61];
    vsi3_databits_idx = 1 + [70 71 82 93 96 97 98 99 100 101 72 73 74 75 76 77 , 78 79 80 81 83 84 85 86 87 88 89 90 91 92 94 95];
    vsi4_databits_idx = 1 + [102 103 114 125 128 129 130 131 132 133 104 105 106 107 108 109 , 110 111 112 113 115 116 117 118 119 120 121 122 123 124 126 127];
end

% Flip so we have bit 31 to 0?
if 0,
    vsi1_databits_idx = fliplr(vsi1_databits_idx);
    vsi2_databits_idx = fliplr(vsi2_databits_idx);
    vsi3_databits_idx = fliplr(vsi3_databits_idx);
    vsi4_databits_idx = fliplr(vsi4_databits_idx);
end

% Convert selected databits into char strings ('11110000100000111100001011111001')
N = size(d,1);
d = char('0' + d);
v1 = d(:,vsi1_databits_idx);
v2 = d(:,vsi2_databits_idx);
v3 = d(:,vsi3_databits_idx);
v4 = d(:,vsi4_databits_idx);

% Align VSI1/2 data with VSI3/4 data (11 clock cycle internal delay in FILA10G capture)
Loffset = 11;
v1 = v1(1:(end-Loffset),:);
v2 = v2(1:(end-Loffset),:);
v3 = v3((Loffset+1):end,:);
v4 = v4((Loffset+1):end,:);
N = N - Loffset;

% Compare
nodiff = m_bitdiff(v1(1,:),v1(1,:));
fo = fopen('vsi_diff.log','w');
for ii=1:N,
    a = v1(ii,:); xa = dec2hex(bin2dec(a));
    b = v2(ii,:); xb = dec2hex(bin2dec(b));
    c = v3(ii,:); xc = dec2hex(bin2dec(c));
    d = v4(ii,:); xd = dec2hex(bin2dec(d));
    fprintf(fo, '--- clk cycle %4d ----------------------|----------|--- diff to vsi1 ----------------\n', ii);
    fprintf(fo, 'vsi 1 : %s : %s : %s\n', a,xa,nodiff);
    fprintf(fo, 'vsi 2 : %s : %s : %s\n', b,xb,m_bitdiff(a,b));
    fprintf(fo, 'vsi 3 : %s : %s : %s\n', c,xc,m_bitdiff(a,c));
    fprintf(fo, 'vsi 4 : %s : %s : %s\n', d,xd,m_bitdiff(a,d));
    fprintf(1, 'Clock cycle %d/%d done.\n', ii,N);
end
fclose(fo);

fprintf(1, 'Wrote results into vsi_diff.log\n');

end


% function ds = m_bitdiff(s1, s2)
% Returns the "difference" between two bits or two N-bit strings.
function ds = m_bitdiff(s1, s2)
    ds = (s1-s2);
    ds(ds == 0) = '.';
    ds(ds == -1) = '-';
    ds(ds == +1) = '+';
    ds = char(ds);
end

