%
% function d = readVCD(fn)
%
% Reads the waveform data from a Value Change Dump (VCD) file
% as produced by, for example, Xilinx ChipScope or VHDL simulators.
%
% Returns a MxN matrix with the raw bit state (0, 1) of all N signals
% in the file, over M clock cylces. Does not parse or return the
% names of the signals.
%
function d = readVCD(fn)

d = [];
fd = fopen(fn, 'r');

% locate first timestamp (line starting with '#<nr>')
while ~feof(fd),
    l = fgetl(fd);
    if (l == -1),
        break;
    end
    if (numel(l) > 0 && l(1)=='#'),
        break;
    end
end

while ~feof(fd),
    newrow = [];
    % read bits under each timestamp
    while ~feof(fd),
        l = fgetl(fd);
        % EOF, append current set of values before quitting
        if (l == -1),
            if (numel(newrow) == size(d,2)),
                d(end+1,:) = newrow;
            end
            break;
        end
        % new timestamp?
        if (numel(l) > 0 && l(1)=='#'),
            d(end+1,:) = newrow;
            fprintf(1, 'Time %s\n', l(2:end));
            continue;
        end
        % data under current timestamp
        % format is "<bitvalue>n<netnr>"
        netnr = str2num(l(3:end)) + 1; % index 0..(N-1) --> 1..N
        bitval = str2num(l(1));
        newrow(netnr) = bitval;
    end
end

fclose(fd);

end
